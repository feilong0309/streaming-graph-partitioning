This is the basic partitioning heuristic implementation. Tested it on kkt_power-in.txt graph

Please have all the dependencies and header files in dep folder (and make sure they have no "main" function)
Any main class has to in the level above and has to be compiled as 

g++ testHeuristic.cpp dep/* -o test_heuristic

Also, make sure that all headers are included as #include "dep/Graph.h"

Had to make these changes for ease of compilation and to reduce "multiple definitions of" errors.

Weight of a partition can now be obtained from Partition.getWeight(). Make sure that the value LINEAR/EXPONENTIAL/UNWEIGHTED is passed during the object creation of Partitioner.
